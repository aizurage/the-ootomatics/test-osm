import React from 'react';


class Map extends React.Component{
  render(){
    return(
      <div>
        <iframe width='640' height='480' frameBorder='0' scrolling="no" marginHeight={0} marginWidth={0} src="https://www.openstreetmap.org/export/embed.html?bbox=139.90469813346866%2C37.36198831674156%2C139.91746544837955%2C37.36824721541424&amp;layer=mapnik"></iframe>
        <br/>
          <small>
            <a href="https://www.openstreetmap.org/#map=17/37.36512/139.91108">大きな地図を表示</a>
            </small>
        </div>
      
    )  
  }
}

export default Map